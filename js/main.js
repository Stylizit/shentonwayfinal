////////////////////////
// VARS
////////////////////////
var height;
var scrollTop;

var isLateralNavAnimating = false;
var positions = [];
var sections = $('.anchor');
var slider = $(gallery).find('.slider');
var images = $(gallery).find('.slider li');
var pagination = $('<ul class="pagination" id="pagination"></ul>');
var galleryWidth;
var galleryHeight;
var nbSections = $(sections).length;
var mWindow = $(window);
var nbSlides = 0;
var currentPage = 0;
var lastScrollTop = 0;
var bodyTimeout = null;
var currentIndex = 0;
/*
var sdegree1 = 0, sdegree2 = 0, sdegree3 = 0, sdegree4 = 0,
    sdegree5 = 0, sdegree6 = 0, sdegree7 = 0;
var of1 = 0, of2 = 0, of3 = 0, of4 = 0;
var ev1 = 0, ev2 = 0, ev3 = 0,
    ev4 = 0, ev5 = 0, ev6 = 0;
var beau1 = 0, beau2 = 0, beau3 = 0;
    beau4 = 0; beau5 = 0, beau6 = 0,
    beau7 = 0, beau8 = 0, beau9 = 0;
var food1 = 0, food2 = 0, food3 = 0,
    food4 = 0, food5 = 0, food6 = 0, food7 = 0;
var heal1 = 0, heal2 = 0, heal3 = 0;
*/

////////////////////////
// SPEED COEFFICIENTS
////////////////////////

var COEFF_CORRECTION = 1.0;
var SCROLL_OFFSET = 400;

// NAVIGATION
var SPEED_NAVIGATION = 500; // ms

// HOME
var SPEED_HOME_LEMONS = 0.75;
var SPEED_HOME_BICYCLES = 0.25;
var SPEED_HOME_IPHONES_OPAQUE = 1;
var SPEED_HOME_LEMONS_OPAQUE = 2;
var SPEED_HOME_IPHONE_OPAQUE_TOP = 0.5;
var SPEED_HOME_IPHONE_OPAQUE_MID = 0.5;
var SPEED_HOME_IPHONE_OPAQUE_BOTTOM = 0.5;

// OFFERING
var SPEED_OFFERING_GLASS_OPAQUE_TOP = 1;
var SPEED_OFFERING_GLASS_OPAQUE_MID = 1;
var SPEED_OFFERING_GLASS_OPAQUE_BOTTOM = 1;
var SPEED_OFFERING_GLASSES = 0.1;

// EVENTS
var SPEED_EVENTS_TRUMPETS_OPAQUE = 1;

var SPEED_EVENTS_TRUMPET_TOP = -2;
var SPEED_EVENTS_TRUMPET_MID = -1.25;
var SPEED_EVENTS_TRUMPET_BOTTOM = -0.5;
var SPEED_EVENTS_SALMONS = 0.125;
var SPEED_EVENTS_HAND = -5;

// BEAUTY
var SPEED_BEAUTY_OBJECT_OPAQUE = 1;

var SPEED_BEAUTY_BRUSH = 2;
var SPEED_BEAUTY_EYELINE = -3;
var SPEED_BEAUTY_ORANGE = 2.5;
var SPEED_BEAUTY_PINK = -1.5;
var SPEED_BEAUTY_MATCH_TOP = 20;
var SPEED_BEAUTY_TUBE_TOP = 0;
var SPEED_BEAUTY_TUBE_RIGHT = 0.25;
var SPEED_BEAUTY_PEN_CAP = -3;

// FOOD
var SPEED_FOOD_FORKS = 0.25;
var SPEED_FOOD_LOBSTERS = 0.75;
var SPEED_FOOD_PINEAPPLES = -0.5;

var SPEED_FOOD_PINEAPPLES_OPAQUE = 0.25;
var SPEED_FOOD_STRAWBERRIES_OPAQUE = 0.75;
var SPEED_FOOD_ASPARAGUS_OPAQUE = 0.25;
var SPEED_FOOD_LEMONS_OPAQUE = -0.5;

var SPEED_FOOD_TUMBLE_OPAQUE = 0.5;

// HEALTH
var SPEED_HEALTH_TENNIS_OPAQUE = 1.5;
var SPEED_HEALTH_SHUTTERCOCKS_OPAQUE = 2;
var SPEED_HEALTH_RACKETS = -0.25;


$(document).ready(function(){
    initScripts();
});

function initScripts(){
    initNavigation();
    //initAnimations();
	$(window).on('load', function() {
    	initStaticAnimations();
	});
    addPagination();
    adjustImages();
    
    initSlider();
    //initMap();
	//initMapSize();
}


function initMap(){
    var mapCanvas = document.getElementById('map-canvas');
    var position = new google.maps.LatLng(1.278509, 103.849053);
    
    // Map options
    var mapOptions = {
      center: position,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(mapCanvas, mapOptions);
    
    // Map styles
    map.set('styles', [
    {
        "featureType": "landscape",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            },
            
            {
                "color": "#ffffff"
            }
        ]
    },
    
    {
        "featureType": "poi",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "simplified"
            },
            
            {
                "color": "#000000"
            }
        ]
    },
    
    {
        "featureType": "road",
        "elementType": "geometry",
        "stylers": [
            {
                "lightness": 100
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffe537"
            },
            {
                "weight": 8.0
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "labels",
        "stylers": [
            {
                "color": "#000000"
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dadada"
            },
            {
                "weight": 5.0
            },
            {
                "visibility": "simplified"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dadada"
            },
            {
                "weight": 3.0
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels",
        "stylers": [
            {
                "color": "#dadada"
            },
            {
                "weight": 3.0
            },
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "lightness": 700
            }
        ]
    },
    {
        "featureType": "transit.station.rail",
        "elementType": "labels.text",
        "stylers": [
            {
                "visibility": "off"
            },
            {
                "lightness": 700
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#c6c6c6"
            }
        ]
    }
]);
    map.set('mapMaker', true);
    
    var icon = 'img/icon-marker.png';
    var marker = new google.maps.Marker({
        position: position,
        map: map,
        title: 'Downtown Gallery',
        icon: icon
    });
    
    if(mWindow.width() > 799){
        mapHeight = $(mapCanvas).height() * 0.8;
        
        // Set height of slider to 80% of section height
        /*$('.mapCanvas').css({
            'height': mapHeight,
            'overflow': 'hidden'
        });*/
        
    }
}


function initMapSize(){
	
}

function initSlider(){
    var gallery = $('.gallery');
    var counter = 0;
	var isSmall = false;
    
    if(mWindow.width() > 799){
		$('.anchor.gallery').css({
				'height': '100%'
			});
        galleryWidth = $(slider).width();
        galleryHeight = $(gallery).height() * 0.8;
        console.log();
        // Set height of slider to 80% of section height
        $(slider).css({
            'height': ($(gallery).height()) * 0.8,
            'overflow': 'hidden'
        });
        $('.gallery .footnote').css({
            'display': 'block'
        });
        $('.full-slider').css({
			'position': 'absolute',
            'width': '80000px'
        });
        $('.full-slider li').css({
            'float': 'left',
            'margin': '0 20px',
            'background-size': '100%'
        });
		$('.full-slider li.heighty').css({
                'background-size': 'auto 100%'
            });
        
        $(images).each(function(index){

            if($(this).hasClass('heighty')){
                $(this).css({
                    'width': galleryWidth,
                    'height': galleryHeight,
                    'background-size': 'auto 100%'
                });
            }
            else{
                $(this).css({
                    'width': galleryWidth,
                    'height': galleryHeight
                });
            }
            if($(this).hasClass('active')){ 
                currentPage = index+1;
            }
            nbSlides++;
        });
        
        $('#currentPage').text(currentPage);
        $('#pages').text(nbSlides);

        $('#prev').click(function(e){
            moveSlide(true, currentPage);
        });
        $('#next').click(function(e){
            moveSlide(false, currentPage);
        });  
    }
    else{
		isSmall = true;
            $('.slider').css({
                'overflow': 'visible'
            });
            $('.full-slider').css({
                'width': '100%'
            });
            $('.full-slider li').css({
                'width': 'auto',
                'height': '300px',
                'margin-bottom': '20px',
                'float': 'none',
                'background-size': 'auto 100%'
            });
			$('.full-slider li.special').css({
                'height': '160px'
            });
            $('.full-slider li.heighty').css({
                'background-size': 'auto 100%'
            });
            $('.gallery .footnote').css({
                'display': 'none'
            });
			$('.anchor.gallery').css({
				'height': 'auto'
			});
        }
    
    $(mWindow).on('resize', function(event){
        // Recalculate correct heights and widths of slider & map
		
        if(mWindow.width() > 799){
			$('.anchor.gallery').css({
				'height': '100%'
			});
			counter++;
            galleryWidth = $(slider).width();
            galleryHeight = ($(gallery).height()) * 0.8;

            $(slider).css({
                'height': galleryHeight,
                'overflow': 'hidden'
            });
            $('.gallery .footnote').css({
                'display': 'block'
            });
            $('.full-slider').css({
				'position': 'absolute',
                'width': '80000px'
            });
            $('.full-slider li').css({
                'float': 'left',
                'margin': '0 20px',
                'background-size': '100%',
				'width': galleryWidth,
							'height': galleryHeight
            });
			$('.full-slider li.heighty').css({
                'background-size': 'auto 100%'
            });

			if(counter == 1 && isSmall == true){
				$(images).each(function(index){
					if($(this).hasClass('heighty')){
						$(this).css({
							'width': galleryWidth,
							'height': galleryHeight,
							'background-size': 'auto 100%'
						});
					}
					else{
						$(this).css({
							'width': galleryWidth,
							'height': galleryHeight
						});
					}

					if($(this).hasClass('active')){ 
						currentPage = index+1;
					}
					nbSlides++;

				});
				
				$('#currentPage').text(currentPage);
			$('#pages').text(nbSlides);

			$('#prev').click(function(e){
				moveSlide(true, currentPage);
			});
			$('#next').click(function(e){
				moveSlide(false, currentPage);
			});  
			}
				
			
        }
        else{
            $('.slider').css({
                'overflow': 'visible',
				'height': 'auto'
            });
            $('.full-slider').css({
				'position': 'static',
                'width': '100%',
				'transform': 'translateX(0)',
				'moz-transform': 'translateX(0)',
				'webkit-transform': 'translateX(0)'
            });
            $('.full-slider li').css({
                'width': 'auto',
                'height': '300px',
                'margin-bottom': '20px',
                'float': 'none',
                'background-size': 'auto 100%'
            });
			$('.full-slider li.special').css({
                'height': '160px'
            });
            $('.full-slider li.heighty').css({
                'background-size': 'auto 100%'
            });
            $('.gallery .footnote').css({
                'display': 'none'
            });
			$('.anchor.gallery').css({
				'height': 'auto'
			});
        }
    });
    
    
}

function moveSlide(moveToLeft, currentIndex){
    var fullSlider = $('.full-slider');
    var effect = ""; 
    var offset;
        
    if(moveToLeft){
        if(currentIndex == 1){
        
        }
        else{
            currentPage--;
            $(".active").removeClass("active");
            $($(images).get(currentPage - 1)).addClass("active");
            $('#currentPage').text(currentPage);
            offset = $('.active').width() + 40;
            effect = "translateX(-" +(0 + offset * (currentPage - 1))+ "px)";
            console.log('offset: ',offset);
            performEffect(fullSlider, effect);
        }
    }
    else{
        if(currentIndex == nbSlides){
            currentPage = 1;
            $(".active").removeClass("active");
            $($(images).get(currentPage - 1)).addClass("active");
            $('#currentPage').text(currentPage);
            offset = $('.active').width() + 40;
            effect = "translateX(" +0 + "px)";
            performEffect(fullSlider, effect);
        }
        else{
            currentPage++;
            $(".active").removeClass("active");
            $($(images).get(currentPage - 1)).addClass("active");
            $('#currentPage').text(currentPage);
            offset = $('.active').width() + 40;
            effect = "translateX(-" +offset * (currentPage - 1) + "px)";
            console.log('offset: ',offset);
            performEffect(fullSlider, effect);
        }
        
    }
    
    
}

function scrollToAnchor(name){
    var aTag = $("section[id='"+ name +"']");
    
    
    $('html,body').animate({
        scrollTop: aTag.offset().top,
        duration: 500,
        easing: 'swing'
    });
    
    
    /*$('html').css({
        'margin-top' : '-'+ (aTag.offset().top) + 'px',
        'transition' : '0.5s',
        '-moz-transition' : '0.5s',
        '-webkit-transition' : '0.5s',
        
        '-webkit-transition-timing-function': 'cubic-bezier(0.82, 0.01, 0.77, 0.78)',
        '-moz-transition-timing-function': 'cubic-bezier(0.82, 0.01, 0.77, 0.78)',
        'transition-timing-function': 'cubic-bezier(0.82, 0.01, 0.77, 0.78)'
    });*/
    
}

function adjustImages(){
    /*
    var flowery = $('.flowery');
    var tabPos = $(flowery).css('background-position').split(" ");
    var tabPos2 = $('.champagne').css('background-position').split(" ");
    var posPx = parseFloat(tabPos2[1].substring(0, tabPos2[1].indexOf("%"))) * mWindow.height() / 100;

    var sizePx = 587 * parseFloat(tabPos[1].substring(0, tabPos[1].indexOf("%"))) / 100;
    var sizePx2 = 429 * parseFloat(tabPos2[1].substring(0, tabPos2[1].indexOf("%"))) / 100;
    
    var delta = posPx - sizePx2 - sizePx / 2;
    console.log('posPx:' +posPx);
    console.log('sizePx:' +sizePx);

    // apply modifications

    $(flowery).css({
        'background-position': tabPos[0]+" "+delta+"px"
    });
    */
}	


function addPagination(){
    
				for(var i = 0; i < nbSections; i++) {
					var linkClass = currentIndex === i ? 'pagination-link active' : 'pagination-link';
                    var idSection = $($(sections).get(i)).attr('id');
					$('<li class="' + linkClass + '"><a href="#' + idSection + '" class="dot nav-link"></a></li>').appendTo( pagination );
				}
                
    
				
				pagination.appendTo( $('#container') );
                pagination.wrap('<nav class="nav-pagination"></nav>');
				var paginationHeight = pagination.height();
    

				$('.dot', pagination).css({
					'-webkit-transition': 'all ' + SPEED_NAVIGATION /1000 + 's',
					'-moz-transition'	: 'all ' + SPEED_NAVIGATION /1000 + 's',
					'-o-transition'		: 'all ' + SPEED_NAVIGATION /1000 + 's',
					'transition'		: 'all ' + SPEED_NAVIGATION /1000 + 's'
				});
				/*
				$('li', pagination).bind( 'click.fsvs', function(e){
					$('.active', pagination).removeClass( 'active' );
					$(this).addClass( 'active' );
                    //console.log($(this).index() - 1);
					slideToIndex( $(this).index() - 1, e );
				});
    */
    $(".nav-link").click(function(e) {
        e.preventDefault();
		var href = $(this).attr('href').replace('#', '');
        console.log(href);
		scrollToAnchor(href);
	});
			
}

function slideToIndex(index, e){
    console.log('sldietoindex:'+index);
    var e = e || false;
    if( ! e && pagination ) {
        $('.active', pagination).removeClass( 'active' );
        $('> *', pagination).eq(index).addClass( 'active' );
    }
    
    //$(".anchor").scrollTop(index*100+'%');
    mWindow.scrollTop(positions[index]);
    console.log(positions[index]);
    console.log(mWindow.scrollTop());
    
    if( bodyTimeout !== null ) {
        currentSlideIndex = index;
        clearTimeout( bodyTimeout );
    }
    bodyTimeout = setTimeout( function(){
        //slideCallback( index );
        bodyTimeout = null;
    }, SPEED_NAVIGATION );
			
}

function initNavigation(){
    ////////////////////////
	// Open/close lateral navigation
    ////////////////////////
    
	$('#trigger-menu').on('click', function(event){
		event.preventDefault();
		// Stop if nav animation is running 
		if(!isLateralNavAnimating) {
			if($(this).parents('.csstransitions').length > 0 ) isLateralNavAnimating = true; 
			
			$('body').toggleClass('nav-is-open');
            $(this).toggleClass('clicked');
			$('.nav-wrapper').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				// Animation is over
				isLateralNavAnimating = false;
			});
		}
	});
    
    ////////////////////////
	// Close fullmenu and redirect to right section
    ////////////////////////
    
    $('.primary-nav a').on('click', function(event){
        var link = $(this).attr('href');
        $('body').toggleClass('nav-is-open');
         $('#trigger-menu').toggleClass('clicked');
    });
}

function initStaticAnimations(){
	var sectionHeight = 0;
	//$('.iphones-lemons').addClass('animate-iphones');
	
	if(!window.mobileAndTabletcheck()){
		// initialize skrollr if the window width is large enough
	  if ($(window).width() > 1024) {

		skrollr.init();
	  }

	  // disable skrollr if the window is resized below 768px wide
	  $(window).on('resize', function () {
		if ($(window).width() <= 1024) {
		  skrollr.init().destroy(); // skrollr.init() returns the singleton created above
		}
		  else{
			  skrollr.init();
		  }
	  });
	}
	
	
	/* Offering */
	var heightHands = $('.full-container').height();
	var posHands = $('.hands-2').position().top - $('.hands-2').height();
	var heightMic = $('.mic-hand').height();

	$('.glass-small-2').css({
		'top': posHands - 0.1 * $('.hands-2').height()
	});
	
	/* Events */
	var posMic = $('.mic-hand').position().top - heightMic;
	$('.trumpet-left-top').css({
		'top': posMic - 0.70 * heightMic
	});
	$('.trumpet-left-mid').css({
		'top': posMic - 0.42 * heightMic
	});
	$('.trumpet-left-bottom').css({
		'top': posMic - 0.25 * heightMic
	});
	
	$('.trumpet-right-top').css({
		'top': posMic - 0.70 * heightMic
	});
	$('.trumpet-right-mid').css({
		'top': posMic - 0.42 * heightMic
	});
	$('.trumpet-right-bottom').css({
		'top': posMic - 0.25 * heightMic
	});
	
	/* Style Fashion */
	// related to bottom pos 
	var posWoman = $('.schmink').height();
	
	for(var i = 1; i < 8; i++){
		var heightHat = $('.hat-'+i).height();
		var coeff = 0;
		if(i == 1){
			coeff = -0.257;
		}
		if(i == 2){
			coeff = -0.359;
		}
		if(i == 3){
			coeff = -0.245;
		}
		if(i == 4){
			coeff = -0.39;
		}
		if(i == 5){
			coeff = -0.135;
		}
		if(i == 6){
			coeff = -0.02;
		}
		if(i == 7){
			coeff = 0.56;
		}
		
		$('.hat-'+i).css({
			'bottom': posWoman - ((8-i) * heightHat * coeff)
		});
	}
	
	
	// Fill positions with all the top & bottom positions from sections
    for(var i = 0; i < nbSections; i++){
        positions[i] = $($(sections).get(i)).offset().top;
        
    }
    
    // Fill last for the last section interval
    positions[nbSections] = positions[nbSections - 1] + positions[1];
	
	$(mWindow).on('scroll', function(event){
        height = mWindow.height();
        scrollTop = mWindow.scrollTop();
    
        var isScrollTop;
        
        // Detect scrollTop or dowm
        if(scrollTop > lastScrollTop){
            isScrollTop = false;
        }
        else{
            isScrollTop = true;
        }
        
        // If we scroll in a section, trigger the right animation

        for(var i = 0; i < nbSections; i++){
            if(scrollTop < positions[i+1] && scrollTop > positions[i]){

                
                currentIndex = i;
                $('.active', pagination).removeClass('active');
                $($('li', pagination).get(currentIndex)).addClass('active');
            }
        }
        
        lastScrollTop = scrollTop;
    });
    
    $(mWindow).on('resize', function(event){
        
		// On resize()
		 heightHands = $('.hands-2').height();
		 posHands = $('.hands-2').position().top ;
		console.log($('.hands-2').position().top);
		 $('.glass-small-2').css({
			'top': posHands - 0.1 * heightHands
		});
		 heightMic = $('.mic-hand').height();
		 posMic = $('.mic-hand').position().top;
		$('.trumpet-left-top').css({
			'top': posMic - 0.25 * heightMic
		});
		$('.trumpet-left-mid').css({
			'top': posMic - -0.03 * heightMic
		});
		$('.trumpet-left-bottom').css({
			'top': posMic - -0.2 * heightMic
		});

		$('.trumpet-right-top').css({
			'top': posMic - 0.25 * heightMic
		});
		$('.trumpet-right-mid').css({
			'top': posMic - -0.03 * heightMic
		});
		$('.trumpet-right-bottom').css({
			'top': posMic - -0.2 * heightMic
		});

		var posWoman = $('.schmink').height();
	
	for(var i = 1; i < 8; i++){
		var heightHat = $('.hat-'+i).height();
		var coeff = 0;
		if(i == 1){
			coeff = -0.257;
		}
		if(i == 2){
			coeff = -0.359;
		}
		if(i == 3){
			coeff = -0.245;
		}
		if(i == 4){
			coeff = -0.39;
		}
		if(i == 5){
			coeff = -0.135;
		}
		if(i == 6){
			coeff = -0.02;
		}
		if(i == 7){
			coeff = 0.56;
		}
		
		$('.hat-'+i).css({
			'bottom': posWoman - ((8-i) * heightHat * coeff)
		});
	}
		
		
        // Recalculate correct positions with all the top & bottom positions from sections
        for(var i = 0; i < nbSections; i++){
            positions[i] = $($(sections).get(i)).offset().top;
            //console.log(positions[i]);
        }
    });
	
	
	// Init home animation onload
	//initHome();
	
	/*
	$(window).on('scroll', function(){
		var tubeScrollTop = $('#tube-anchor').offset().top;
		if($(window).scrollTop() >= tubeScrollTop){
			if(!$('.svg-container').hasClass('start-animation')){
				$('.svg-container').addClass('start-animation');
			}
		}
	});
	*/
	
}

function initHome(){
				   
}

function initAnimations(){
    ////////////////////////
    // Scrolling parallax
    ////////////////////////
    
    
    // Fill positions with all the top & bottom positions from sections
    for(var i = 0; i < nbSections; i++){
        positions[i] = $($(sections).get(i)).offset().top;
        console.log(positions[i]);
    }
    
    // Fill last for the last section interval
    positions[nbSections] = positions[nbSections - 1] + positions[1];
    
    $(mWindow).on('scroll', function(event){
        height = mWindow.height();
        scrollTop = mWindow.scrollTop();
    
        var isScrollTop;
        
        // Detect scrollTop or dowm
        if(scrollTop > lastScrollTop){
            isScrollTop = false;
        }
        else{
            isScrollTop = true;
        }
        
        // If we scroll in a section, trigger the right animation
        console.log(isScrollTop);
        for(var i = 0; i < nbSections; i++){
            if(scrollTop < positions[i+1] && scrollTop > positions[i] - SCROLL_OFFSET){
                // Animate on certain screen sizes
                // If window width >= 800
                if(mWindow.width() > 799){
                    triggerAnimation(i, positions[i], isScrollTop);
                }
                currentIndex = i;
                $('.active', pagination).removeClass('active');
                $($('li', pagination).get(currentIndex)).addClass('active');
                console.log(scrollTop);
                console.log('from: '+positions[i]);
                console.log('to: '+positions[i+1]);
            }
        }
        
        lastScrollTop = scrollTop;
    });
    
    $(mWindow).on('resize', function(event){
        
        // Recalculate correct positions with all the top & bottom positions from sections
        for(var i = 0; i < nbSections; i++){
            positions[i] = $($(sections).get(i)).offset().top;
            console.log(positions[i]);
        }
    });
}
    
    
// Trigger right animation
function triggerAnimation(num, position, scrollTop){
    console.log('section number: '+num);
    
    
    // Home
    if(num == 0){
        animateHome(position, scrollTop);
    }
	/*
    // Offering
    if(num == 1){
        animateOffering(position, scrollTop);
    }
    // Events
    if(num == 2){
        animateEvents(position, scrollTop);
    }
    // Beauty
    if(num == 4){
        animateBeauty(position, scrollTop);
    }
    // Food
    if(num == 5){
        animateFood(position, scrollTop);
    }  
    // Health
    if(num == 6){
        animateHealth(position, scrollTop);
    }  
	*/
}
function animateHome(pos, scrollTop){
    if(scrollTop){
        sdegree1 = sdegree1 - SPEED_HOME_LEMONS - (pos * scrollTop); // lemons
        sdegree2 = sdegree2 - SPEED_HOME_BICYCLES - (pos * scrollTop); // bicycles
        sdegree5 = sdegree5 - SPEED_HOME_IPHONE_OPAQUE_TOP * COEFF_CORRECTION - (pos * scrollTop); // iphone top
        sdegree6 = sdegree6 - SPEED_HOME_IPHONE_OPAQUE_MID * COEFF_CORRECTION - (pos * scrollTop); // iphone mid
        sdegree7 = sdegree7 - SPEED_HOME_IPHONE_OPAQUE_BOTTOM * COEFF_CORRECTION - (pos * scrollTop); // iphone bottom
    }
    else{
        sdegree1 = sdegree1 + SPEED_HOME_LEMONS - (pos * scrollTop); // lemons
        sdegree2 = sdegree2 + SPEED_HOME_BICYCLES - (pos * scrollTop); // bicycles
        sdegree5 = sdegree5 + SPEED_HOME_IPHONE_OPAQUE_TOP - (pos * scrollTop); // iphone top
        sdegree6 = sdegree6 + SPEED_HOME_IPHONE_OPAQUE_MID - (pos * scrollTop); // iphone mid
        sdegree7 = sdegree7 + SPEED_HOME_IPHONE_OPAQUE_BOTTOM - (pos * scrollTop); // iphone bottom
    }

    var rotateLemons = "rotate(" + sdegree1 + "deg)";
    var rotateBicycles = "rotate(" + sdegree2 + "deg)";
    var tumbleIphoneTop = 
        "rotateX(" + sdegree5 + "deg) rotateZ(" + sdegree5 + "deg)"
    ;
    var tumbleIphoneMid = 
        "rotateX(" + sdegree6 + "deg) rotateZ(" + sdegree6 + "deg)"
    ; 
    var tumbleIphoneBottom = 
        "rotateX(" + sdegree7 + "deg) rotateZ(" + sdegree7 + "deg)"
    ; 

    var iphonesLemons = $('.iphones-lemons');
    var glassBicycles = $('.glass-bicycles');
    var iphoneTop = $('.iphone-opaque-top');
    var iphoneMid = $('.iphone-opaque-middle');
    var iphoneBottom = $('.iphone-opaque-bottom');
    
    performEffect(iphonesLemons, rotateLemons);
    performEffect(glassBicycles, rotateBicycles);
    performEffect(iphoneTop, tumbleIphoneTop);
    performEffect(iphoneMid, tumbleIphoneMid);  
    performEffect(iphoneBottom, tumbleIphoneBottom);  
    
    
    //sdegree3 = sdegree3 - SPEED_HOME_IPHONES_OPAQUE - pos; // iphones opaque
    //sdegree4 = sdegree4 - SPEED_HOME_LEMONS_OPAQUE - pos; // lemons opaque
    //sdegree3 = sdegree3 + SPEED_HOME_IPHONES_OPAQUE - pos; // iphones opaque
    //sdegree4 = sdegree4 + SPEED_HOME_LEMONS_OPAQUE - pos; // lemons opaque
    //var rotateIphonesOpaque = "rotate(" + sdegree3 + "deg)";
    //var rotateLemonsOpaque = "rotate(" + sdegree4 + "deg)";
    //var iphonesOpaque = $('.iphones-opaque');
    //var lemonsOpaque = $('.lemons-opaque');
    //performRotation(iphonesOpaque, rotateIphonesOpaque);
    //performRotation(lemonsOpaque, rotateLemonsOpaque);

}

function animateOffering(pos, scrollTop){
    if(scrollTop){
        of1 = of1 - SPEED_OFFERING_GLASS_OPAQUE_TOP - (pos * (scrollTop - scrollTop));
        of2 = of2 - SPEED_OFFERING_GLASS_OPAQUE_MID - (pos * (scrollTop - scrollTop)); 
        of3 = of3 - SPEED_OFFERING_GLASS_OPAQUE_BOTTOM  - (pos * (scrollTop - scrollTop)); 
        of4 = of4 - SPEED_OFFERING_GLASSES  - (pos * (scrollTop - scrollTop)); 
    }
    else{
        of1 = of2 + SPEED_OFFERING_GLASS_OPAQUE_TOP - (pos * scrollTop); 
        of2 = of2 + SPEED_OFFERING_GLASS_OPAQUE_MID - (pos * scrollTop); 
        of3 = of3 + SPEED_OFFERING_GLASS_OPAQUE_BOTTOM - (pos * scrollTop); 
        of4 = of4 + SPEED_OFFERING_GLASSES - (pos * scrollTop); 
    }
    
    var rotateGlass = 
        "rotate(" + of4 + "deg)"
    ;
    var tumbleTop = 
        "rotateX(" + of1 + "deg) rotateZ(" + of1 + "deg)"
    ;
    var tumbleMid = 
        "rotateX(" + of2 + "deg) rotateZ(" + of2 + "deg)"
    ; 
    var tumbleBottom = 
        "rotateX(" + of3 + "deg) rotateZ(" + of3 + "deg)"
    ; 

    var glassSmall = $('.glass-small');
    var glassTop = $('.glass-opaque-top');
    var glassMid = $('.glass-opaque-middle');
    var glassBottom = $('.glass-opaque-bottom');

    performEffect(glassSmall, rotateGlass);
    performEffect(glassTop, tumbleTop);
    performEffect(glassMid, tumbleMid);  
    performEffect(glassBottom, tumbleBottom);  
    

}

function animateEvents(pos, scrollTop){
    if(scrollTop){
        
        ev1 = ev1 - SPEED_EVENTS_TRUMPETS_OPAQUE - (pos * (scrollTop - scrollTop));
        ev2 = ev2 - SPEED_EVENTS_TRUMPET_TOP - (pos * (scrollTop - scrollTop));
        ev3 = ev3 - SPEED_EVENTS_TRUMPET_MID - (pos * (scrollTop - scrollTop)); 
        ev4 = ev4 - SPEED_EVENTS_TRUMPET_BOTTOM  - (pos * (scrollTop - scrollTop)); 
        ev5 = ev5 - SPEED_EVENTS_SALMONS  - (pos * (scrollTop - scrollTop)); 
        ev6 = ev6 - SPEED_EVENTS_HAND  - (pos * (scrollTop - scrollTop)); 
    }
    else{
        ev1 = ev1 + SPEED_EVENTS_TRUMPETS_OPAQUE - (pos * scrollTop); 
        ev2 = ev2 + SPEED_EVENTS_TRUMPET_TOP - (pos * scrollTop); 
        ev3 = ev3 + SPEED_EVENTS_TRUMPET_MID - (pos * scrollTop); 
        ev4 = ev4 + SPEED_EVENTS_TRUMPET_BOTTOM - (pos * scrollTop);
        ev5 = ev5 + SPEED_EVENTS_SALMONS - (pos * scrollTop);
        ev6 = ev6 + SPEED_EVENTS_HAND - (pos * scrollTop);
    }
    
    var tumbleOpaque = 
        "rotateX(" + ev1 + "deg) rotateZ(" + ev1 + "deg)"
    ;
    
    var tumbleLeftTop = 
        "translateX(" + Math.ceil(ev2) + "px)"
    ;
    var tumbleLeftMid = 
        "translateX(" + Math.ceil(ev3) + "px)"
    ; 
    var tumbleLeftBottom = 
        "translateX(" + Math.ceil(ev4) + "px)"
    ;
    var tumbleRightTop = 
        "translateX(" + -Math.ceil(ev2) + "px)"
    ;
    var tumbleRightMid = 
        "translateX(" + -Math.ceil(ev3) + "px)"
    ; 
    var tumbleRightBottom = 
        "translateX(" + -Math.ceil(ev4) + "px)"
    ;
    var tumbleSalmons = 
        "rotate(" + ev5 + "deg)"
    ; 
    var moveHand = 
        "translateY(" + ev6 + "px)"
    ;

    var trumpetsOpaque = $('.trumpets');
    var trumpetLeftTop = $('.trumpet-left-top');
    var trumpetLeftMid = $('.trumpet-left-mid');
    var trumpetLeftBottom = $('.trumpet-left-bottom');
    var trumpetRightTop = $('.trumpet-right-top');
    var trumpetRightMid = $('.trumpet-right-mid');
    var trumpetRightBottom = $('.trumpet-right-bottom');
    var salmons = $('.salmons');
    var hand = $('.mic-hand');

    performEffect(trumpetsOpaque, tumbleOpaque);
    performEffect(trumpetLeftTop, tumbleLeftTop);
    performEffect(trumpetLeftMid, tumbleLeftMid);  
    performEffect(trumpetLeftBottom, tumbleLeftBottom);  
    performEffect(trumpetRightTop, tumbleRightTop);
    performEffect(trumpetRightMid, tumbleRightMid);  
    performEffect(trumpetRightBottom, tumbleRightBottom);  
    performEffect(salmons, tumbleSalmons);
    performEffect(hand, moveHand);  
}


function animateBeauty(pos, scrollTop){
    if(scrollTop){
        beau1 = beau1 - SPEED_BEAUTY_OBJECT_OPAQUE - (pos * (scrollTop - scrollTop));
        beau2 = beau2 - SPEED_BEAUTY_BRUSH - (pos * (scrollTop - scrollTop));
        beau3 = beau3 - SPEED_BEAUTY_EYELINE - (pos * (scrollTop - scrollTop));
        beau4 = beau4 - SPEED_BEAUTY_ORANGE - (pos * (scrollTop - scrollTop));
        beau5 = beau5 - SPEED_BEAUTY_PINK - (pos * (scrollTop - scrollTop));
        beau6 = beau6 - SPEED_BEAUTY_MATCH_TOP - (pos * (scrollTop - scrollTop));
        beau7 = beau7 - SPEED_BEAUTY_TUBE_TOP - (pos * (scrollTop - scrollTop));
        beau8 = beau8 - SPEED_BEAUTY_TUBE_RIGHT - (pos * (scrollTop - scrollTop));
        beau9 = beau9 - SPEED_BEAUTY_PEN_CAP - (pos * (scrollTop - scrollTop));
    }
    else{
        beau1 = beau1 + SPEED_BEAUTY_OBJECT_OPAQUE - (pos * scrollTop); 
        beau2 = beau2 + SPEED_BEAUTY_BRUSH - (pos * scrollTop); 
        beau3 = beau3 + SPEED_BEAUTY_EYELINE - (pos * scrollTop); 
        beau4 = beau4 + SPEED_BEAUTY_ORANGE - (pos * scrollTop); 
        beau5 = beau5 + SPEED_BEAUTY_PINK - (pos * scrollTop); 
        beau6 = beau6 + SPEED_BEAUTY_MATCH_TOP - (pos * scrollTop); 
        beau7 = beau7 + SPEED_BEAUTY_TUBE_TOP - (pos * scrollTop); 
        beau8 = beau8 + SPEED_BEAUTY_TUBE_RIGHT - (pos * scrollTop); 
        beau9 = beau9 + SPEED_BEAUTY_PEN_CAP - (pos * scrollTop); 
    }
    
    var tumbleMakeup = 
        "rotateX(" + beau1 + "deg) rotateZ(" + beau1 + "deg)"
    ;
    
    var rotateBrush = 
        "translateY(" + beau2 + "px)"
    ;
    var rotateEyeliner = 
        "translateY(" + beau3 + "px)"
    ;
    var rotateMakeupOrange = 
        "translateY(" + beau4 + "px)"
    ;
    var rotateMakeupPink = 
        "translateY(" + beau5 + "px)"
    ;
    var rotateMatchTop = 
        "translateY(" + beau6 + "px)"
    ;
    var rotateTubeTop = 
        "translateY(" + beau7 + "px)"
    ;
    var rotateTubeRight = 
        "translateY(" + beau8 + "px)"
    ;
    var rotatePenCap = 
        "translateY(" + beau9 + "px)"
    ;
    

    var makeup = $('.makeup');
    var brush = $('.brush');
    var eyelinerLeft = $('.eyeliner-left');
    var makeupOrange = $('.makeup-orange');
    var makeupPink = $('.makeup-pink');
    var matchTop = $('.match-top');
    var tubeTop = $('.tube-top');
    var tubeRight = $('.tube-right');
    var penCap = $('.pen-cap');
    
    performEffect(makeup, tumbleMakeup);
    performEffect(brush, rotateBrush);
    performEffect(eyelinerLeft, rotateEyeliner);
    performEffect(makeupOrange, rotateMakeupOrange);
    performEffect(makeupPink, rotateMakeupPink);
    performEffect(matchTop, rotateMatchTop);
    performEffect(tubeTop, rotateTubeTop);
    performEffect(tubeRight, rotateTubeRight);
    performEffect(penCap, rotatePenCap);
}


function animateFood(pos, scrollTop){
    if(scrollTop){
        food1 = food1 - SPEED_FOOD_FORKS - (pos * (scrollTop - scrollTop));
        food2 = food2 - SPEED_FOOD_LOBSTERS - (pos * (scrollTop - scrollTop)); 
        food3 = food3 - SPEED_FOOD_PINEAPPLES - (pos * (scrollTop - scrollTop)); 
        
        food4 = food4 - SPEED_FOOD_PINEAPPLES_OPAQUE - (pos * (scrollTop - scrollTop));
        food5 = food5 - SPEED_FOOD_STRAWBERRIES_OPAQUE - (pos * (scrollTop - scrollTop)); 
        food6 = food6 - SPEED_FOOD_LEMONS_OPAQUE - (pos * (scrollTop - scrollTop));
        food7 = food7 - SPEED_FOOD_ASPARAGUS_OPAQUE - (pos * (scrollTop - scrollTop)); 
    }
    else{
        food1 = food1 + SPEED_FOOD_FORKS - (pos * scrollTop); 
        food2 = food2 + SPEED_FOOD_LOBSTERS - (pos * scrollTop); 
        food3 = food3 + SPEED_FOOD_PINEAPPLES - (pos * scrollTop); 
              
        food4 = food4 + SPEED_FOOD_PINEAPPLES_OPAQUE - (pos * scrollTop); 
        food5 = food5 + SPEED_FOOD_STRAWBERRIES_OPAQUE - (pos * scrollTop); 
        food6 = food6 + SPEED_FOOD_LEMONS_OPAQUE - (pos * scrollTop); 
        food7 = food7 + SPEED_FOOD_ASPARAGUS_OPAQUE - (pos * scrollTop); 
    }
    
    var rotateForks = 
        "rotate(" + food1 + "deg)"
    ;
    var rotateLobsters = 
        "rotate(" + food2 + "deg)"
    ;
    var rotatePineapples = 
        "rotate(" + food3 + "deg)"
    ;
    
    var rotateLemons = 
        "rotate(" + food4 + "deg)"
    ;
    var rotateStrawberries = 
        "rotate(" + food5 + "deg)"
    ;
    var RotatePineapplesOpaque = 
        "rotate(" + food6 + "deg)"
    ;
    var RotateAsparagusOpaque = 
        "rotate(" + food7 + "deg)"
    ;


    var forks = $('.forks');
    var lobsters = $('.lobsters');
    var pineapples = $('.pineapples-leaves');
    
    var lemons = $('.lemons-small-opaque');
    var strawberries = $('.strawberries-opaque');
    var pineapplesOpaque = $('.pineapples-opaque');
    var asparagusOpaque = $('.asparagus-opaque');
    
    performEffect(forks, rotateForks);
    performEffect(lobsters, rotateLobsters);
    performEffect(pineapples, rotatePineapples);
    performEffect(lemons, rotateLemons);
    performEffect(strawberries, rotateStrawberries);
    performEffect(pineapplesOpaque, RotatePineapplesOpaque);
    performEffect(asparagusOpaque, RotateAsparagusOpaque);

}


function animateHealth(pos, scrollTop){
    if(scrollTop){
        heal1 = heal1 - SPEED_HEALTH_TENNIS_OPAQUE - (pos * (scrollTop - scrollTop));
        heal2 = heal2 - SPEED_HEALTH_SHUTTERCOCKS_OPAQUE - (pos * (scrollTop - scrollTop));
        heal3 = heal3 - SPEED_HEALTH_RACKETS - (pos * (scrollTop - scrollTop));
    }
    else{
        heal1 = heal1 + SPEED_HEALTH_TENNIS_OPAQUE - (pos * scrollTop); 
        heal2 = heal2 + SPEED_HEALTH_SHUTTERCOCKS_OPAQUE - (pos * scrollTop); 
        heal3 = heal3 + SPEED_HEALTH_RACKETS - (pos * scrollTop);
    }
    
    var rotateTennis = 
        "rotateZ(" + heal1 + "deg)"
    ;
    var rotateShutterCocks = 
        "rotateZ(" + heal2 + "deg)"
    ;
    var rotateRackets = 
        "rotateZ(" + heal3 + "deg)"
    ;

    var tennis = $('.tennis');
    var shuttercocks = $('.shuttercock');
    var rackets = $('.rackets');
    
    performEffect(tennis, rotateTennis); 
    performEffect(shuttercocks, rotateShutterCocks);
    performEffect(rackets, rotateRackets);
}

// Effect helper
function performEffect(div, effect){
    
    $(div).css({
        "-webkit-transform": effect,  
        "-moz-transform": effect,
        "-o-transform": effect,
        "-ms-transform": effect,
        "transform": effect
    });
}